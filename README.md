## Introducción

Este es un ejercicio colaborativo, en el que utilizarás por primera vez una
plataforma de control de versiones con [Git](https://git-scm.com/book/en/v2).

El objetivo es crear un libro en [LaTeX](https://www.latex-project.org/) que
contenga un capítulo escrito por cada uno de los estudiantes de Ciencias de
Computación de la generación 2024. En este capítulo puedes escribir una breve
presentación de ti mismo, incluyendo alguna ilustración que puede ser una foto
de ti o algo que consideres que te representa. Esperamos que este ejercicio
ayude a que la generación 2024 se conozca e identifique más fácilmente. Si no
sabes qué escribir en tu capítulo, aquí hay algunas ideas (sólo escribe la
información con la que te sientas cómodo/a de compartir):

- Nombre y edad.
- De dónde eres.
- Por qué elegiste estudiar Ciencias de la Computación.
- Qué te gusta hacer en tu tiempo libre.
- Alguna afición.
- Acerca de tus mascotas.

## Instrucciones

1. Ve [al repositorio del proyecto
   coolaborativo](https://gitlab.com/CComputacion/generacion2024) y haz clic en
   **Request Access**.

   ![gitlab1.png](https://gitlab.com/CComputacion/generacion2024/-/wikis/uploads/c2f41c27706434ddf6d5084fb7505c37/gitlab1.png)

2. Ahora haz clic en el botón **Clone** y copia la URL **HTTPS** para clonar el
   repositorio desde la terminal.

   ![gitlab2.png](https://gitlab.com/CComputacion/generacion2024/-/wikis/uploads/00e5f911f3a6e49eecf29e44eb05342a/gitlab2.png)

3. Ve a la terminal y clona (`clone`) la URL que acabas de copiar.

    ```sh
    git clone https://gitlab.com/CComputacion/generacion2024.git
    ```

    y entra al directorio raíz del repositorio:

    ```sh
    cd generacion2024/
    ```

    A partir de este momento todas las instrucciones que mostremos de la línea
    de comandos se espera que las ejecutes en el directorio raíz del repositorio
    (`generacion2024`).

4. Crea una rama (`branch`) que tenga como nombre tu número de cuenta y pasa a
   trabajar en ella (`checkout`).

    ```sh
    git branch [no. cuenta]
    git checkout [no. cuenta]
    ```

5. Abre [tu editor](https://www.gnu.org/software/emacs/) y edita el archivo con
   extensión `tex` que se encuentra dentro del directorio que se llama como tu
   número de cuenta. También puedes agregar las ilustraciones que necesites para
   tu texto; sólo recuerda mantenerlas dentro de tu directorio, que corresponde
   a tu capítulo.

   El archivo `tex` *principal* del documento está en la raíz del mismo (se
   llama `generacion2024.tex`). El directorio con tu capítulo y tus imágenes (si
   decides agregarlas) está también en la raíz; pero entonces dicho capítulos y
   dichas imágenes están “debajo” del documento *principal*, que es el que se
   compila. Por lo tanto, para referir a tus imágenes, tienes que usar la ruta
   relativa de la misma, que incluye el directorio que se llama como tu número
   de cuenta.

   En otras palabras, para que el estudiante con número de cuenta *123456789*
   agregue una imagen llamada `foto.jpg`, en el archivo `123456789.tex` debe
   usarse:

   ```tex
   \includegraphics[width=0.8\textwidth]{123456789/foto.jpg}
   ```

6. Verifica que el documento compila sin errores:

   ```sh
   pdflatex generacion2024.tex
   ```

   y que puedes visualizar los cambios que hayas hecho:

   ```sh
   evince generacion2024.pdf
   ```

6. Cuando estés satisfecho con tus cambios, salva los mismos en tu editor,
   agrega (`add`) los cambios a Git y guárdalos (`commit`):

    ```sh
    git add [no. cuenta]
    git commit -m "Mi capítulo está listo."
    ```

7. Ahora empuja (`push`) tus cambios al repositorio en línea.

    ```sh
    git push origin [no. cuenta]
    ```

8. Ve a
   [`https://gitlab.com/CComputacion/generacion2024`](https://gitlab.com/CComputacion/generacion2024)
   y selecciona la rama que corresponde a tu capítulo.

   ![gitlab3.png](https://gitlab.com/CComputacion/generacion2024/-/wikis/uploads/3e23adec9f3b51ee60d6e55b52ecb899/gitlab3.png)

9. Ahora haz clic en el botón **Create merge request**.

   ![gitlab4.png](https://gitlab.com/CComputacion/generacion2024/-/wikis/uploads/63a1c4bfcc5c2e7e63ed10ede422336a/gitlab4.png)

10. El título de tu solicitud de mezcla (`merge request`) es, por omisión, el
    comentario del último cambio guardado en tu rama en Git; agregar una
    descripción o comentarios es opcional. No es necesario que edites nada.

11. Ve a la parte inferior de la página de creación de nueva solicitud de mezcla
    y haz clic en **Create merge request**.

    ![gitlab5.png](https://gitlab.com/CComputacion/generacion2024/-/wikis/uploads/646b53b22504218066c72ce1b735a38d/gitlab5.png)

12. Ahora espera a que los tutores del propedéutico acepten las solicitudes de
    mezcla; te avisaremos por correo cuando el libro esté listo.

Recuerda no modificar nada fuera del directorio con tu número de cuenta. 🙂
